function [C, sigma] = dataset3Params(X, y, Xval, yval)
%EX6PARAMS returns your choice of C and sigma for Part 3 of the exercise
%where you select the optimal (C, sigma) learning parameters to use for SVM
%with RBF kernel
%   [C, sigma] = EX6PARAMS(X, y, Xval, yval) returns your choice of C and 
%   sigma. You should complete this function to return the optimal C and 
%   sigma based on a cross-validation set.
%

% You need to return the following variables correctly.
C = 1;
sigma = 0.3;

% ====================== YOUR CODE HERE ======================
% Instructions: Fill in this function to return the optimal C and sigma
%               learning parameters found using the cross validation set.
%               You can use svmPredict to predict the labels on the cross
%               validation set. For example, 
%                   predictions = svmPredict(model, Xval);
%               will return the predictions on the cross validation set.
%
%  Note: You can compute the prediction error using 
%        mean(double(predictions ~= yval))
%

	%coptions=[0.01,0.03,0.1,0.3,1,3,10,30];

	%sigmaoptions=[0.01,0.03,0.1,0.3,1,3,10,30];
	%cl = length(coptions);
	%sigmal=length(sigmaoptions);
		
	%R = zeros(cl^2,3);
	
	%index=1;
	%for cval = coptions
	%	for sigmaval = sigmaoptions
	%		model= svmTrain(X, y, cval, @(x1, x2) gaussianKernel(x1, x2, sigmaval)); 
	%		predictions = svmPredict(model,Xval);
	%		preerr = mean(double(predictions ~= yval));
			
	%		R(index,:)=[preerr,cval,sigmaval];
	%		index=index+1;
	%	end
	%end
			

	%[minerr,errindex]=min(R(:,1));

	%	minrow = R(errindex,:);
		
	%	C=minrow(2);
	%	sigma=minrow(3);
	%comment out the code to find optimal C and sigma
	C = 1.0;
	sigma=0.1;	


% =========================================================================

end
